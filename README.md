# includes-theme

This sample shows how a theme is created with freemarker templates. It also shows how to add custom theme settings, custom portlets and how to iterate through a list of articles and configure Web Content Display Portlets to show the current article from the list. The articles were selected via JournalArticleLocalService.

Also there are some examples regarding staticUtil and one on how to get / use serviceContext.

Please have also a look into the siblings project [service-context-contributor](../../../service-context-contributor)

## Getting Started

This project was generated with 

	blade create -b maven -t theme includes-theme 

All comments in the code start with a ``COMMENT:`` so they can easily be found via search function.

### Prerequisites

* Running Liferay 7.1 CE
* Maven 3
* Java 8

### Installing

Build with

	# mvn clean package

Deploy it through copying into ``LIFERAY_HOME/deploy``

Replace ``LIFERAY_HOME`` with the absolute path to your liferay home folder.

### Extended Files

* [pom.xml](pom.xml) - added a hint how to extend from ``_unstyled`` theme instead of ``_styled``
* [init_custom.ftl](src/main/webapp/templates/init_custom.ftl) - loads the custom theme settings 
* [portal_normal.ftl](src/main/webapp/templates/portal_normal.ftl) - the main file for the theme 

## Built With

* [Blade](https://dev.liferay.com/develop/tutorials/-/knowledge_base/7-1/blade-cli) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Liferay](https://dev.liferay.com/) - Portal / runtime container

## Further information

* [Howto use staticUtil in themes](https://stackoverflow.com/questions/37341835/freemarker-does-not-assign-staticutil)
* [Howto use ServiceLocator in themes](https://community.liferay.com/forums/-/message_boards/message/73386692)
* [When to use ServiceLocator vs. staticUtil](https://community.liferay.com/forums/-/message_boards/message/94828121)
* [Not all classes available in themes via staticUtil](https://community.liferay.com/forums/-/message_boards/message/111698309)

## Contributing

No contributing since this is a sample project.

## Versioning

No versioning since this is a sample project.

## Authors

* **Manuel Manhart** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
