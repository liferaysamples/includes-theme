<!DOCTYPE html>

<#-- COMMENT: changes start -->

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="google" content="notranslate">
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>${custom_site_name}</title>

	<@liferay_util["include"] page=top_head_include />
</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<div id="wrapper" class="container-fluid">
	<div class="site-wrapper">
		<header id="banner" role="banner" class="site-header hidden-print">
			<#if has_navigation && is_setup_complete>
				<div class="nav-main">
<#--
					COMMENT: disable default menu for demo purposes and add a menu portlet instead
 -->							
<#-- 
					<#include "${full_templates_path}/navigation.ftl" />
-->
					<div class="nav-main-content-sub">
						<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "barebone") />
						<#assign VOID = freeMarkerPortletPreferences.setValue("displayStyle", "ddmTemplate_NAV_MAIN_SUB") />
						<#assign VOID = freeMarkerPortletPreferences.setValue("displayStyleGroupId",  "${group_id}") />
						<#assign VOID = freeMarkerPortletPreferences.setValue("displayDepth", "2") />
						<#assign VOID = freeMarkerPortletPreferences.setValue("rootLayoutLevel", "0") />
						<#assign VOID = freeMarkerPortletPreferences.setValue("rootLayoutType", "relative") />
						<#assign VOID = freeMarkerPortletPreferences.setValue("includedLayouts", "all") />
						<#assign VOID = freeMarkerPortletPreferences.setValue("nestedChildren", "false") />
			
						<@liferay.navigation_menu default_preferences="${freeMarkerPortletPreferences}" instance_id="navMainSub" />
						<#assign VOID = freeMarkerPortletPreferences.reset() />
					</div>
				</div>
				<div class="nav-main-content-footer site-footer-bottom">
					
					<#-- COMMENT: inject a liferay service start -->
					<#assign journalArticleService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService") />
					<#assign articles = journalArticleService.getArticles(group_id) />
					<div class="theme-articles">
						<h2>List some articles in the theme</h2>

						<#list articles as item>
						
							<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "barebone") />
							<#assign theme_groupID = htmlUtil.escape(theme_display.getCompanyGroupId()?string) />
							<#assign VOID = freeMarkerPortletPreferences.setValue("groupId", '${group_id}') />
							<#assign VOID = freeMarkerPortletPreferences.setValue("articleId", "${item.articleId}") />
							
					        <@liferay_portlet["runtime"]
					        defaultPreferences="${freeMarkerPortletPreferences}"
					        portletProviderAction=portletProviderAction.VIEW
					        instanceId="article_${item.articleId}"
					        portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet" />
					        ${freeMarkerPortletPreferences.reset()}
	
						</#list>
					</div>
					<#-- COMMENT: inject a liferay service end -->
					
					<#-- COMMENT: inject a staticUtil class start -->

					<#-- COMMENT: fetch a class via static util example. For this to work, you need to add following two lines in your
						portal-ext.properties and restart the server:

						freemarker.engine.restricted.classes=java.lang.ClassLoader
						freemarker.engine.restricted.packages=
						freemarker.engine.restricted.variables=
					 -->
					<#assign 
						textFormatter = staticUtil["com.liferay.portal.kernel.util.TextFormatter"]
					/>
					<p>
						text formatter: ${textFormatter.formatName("Test String")}
					</p>

					<#-- COMMENT: if class ServiceContextThreadLocal is not available via staticUtil, this is an alternative way.
						For this to work you need to checkout and deploy the siblings project service-context-contributor.
					 -->
					<p>
						serviceContext.getCompanyId: 
						<#if serviceContext??>
							${serviceContext.getCompanyId()}
						<#else>
							serviceContext is not in context
						</#if>
					</p>

					
					<#-- COMMENT: should work, but in my case the class could not be found, if its not working see alternative way above -->
					<#assign serviceContextThreadLocal = staticUtil["com.liferay.portal.kernel.service.ServiceContextThreadLocal"] />
					<p>
						<#if serviceContextThreadLocal??>
							<p>serviceContextThreadLocal.getServiceContext.getCompanyId: ${serviceContextThreadLocal.getServiceContext().getCompanyId()}</p>
						<#else>
							serviceContextThreadLocal from staticUtil is not in context
						</#if>
					</p>

					<#-- COMMENT: inject a staticUtil class end -->
	
				</div>
			</#if>
		</header>
	
		<section id="content" class="site-content">
			<div class="site-content-container">
				<h1 class="hide-accessible">${custom_site_name}</h1>
				<#-- COMMENT: include the content layout template -->	
				<@liferay_util["include"] page=content_include />
			</div>
		</section>
	
		<footer id="footer" class="site-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<@navFooter instance_id="navFooterDoormatSiteFooter" template_id="ddmTemplate_NAV_FOOTER_DOORMAT"/>
					</div>
					<div class="col-md-3">
						<div>
							Kontakt
							<#-- COMMENT: example of (formatting) function call start -->
							<a class="link-inverse" href="tel:${cleanTelNumber(contact_phone_number)}" title="Kontakt">${contact_phone_number}</a>
							<#-- COMMENT: example of (formatting) function call end -->
						</div>
						<div class="site-footer-bottom-container site-footer-copyright">
							&copy; ${the_year} Test AG
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

<!-- inject:js -->
<!-- endinject -->

</body>

</html>

<#-- COMMENT: define callable function start -->

<#function cleanTelNumber telNumber>
	<#assign
	cleanNumber = telNumber?replace("^[^+]?|[^0-9]+", "", "r")
	>
	<#return cleanNumber>
</#function>

<#-- COMMENT: define callable function end -->

<#-- COMMENT: define custom macro start -->

<#macro navFooter
	instance_id = ""
	template_id = ""
	decorator_id = "Borderless"
	custom_title = "Footer Menu"
	>
	<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "${decorator_id}") />
	<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupUseCustomTitle", "true") />
	<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupTitle_en_US", "${custom_title}") />
	<#assign VOID = freeMarkerPortletPreferences.setValue("displayStyle", "${template_id}") />
	<#assign VOID = freeMarkerPortletPreferences.setValue("displayStyleGroupId",  "${group_id}") />
	<#assign VOID = freeMarkerPortletPreferences.setValue("displayDepth", "1") />
	<#assign VOID = freeMarkerPortletPreferences.setValue("rootLayoutLevel", "0") />
	<#assign VOID = freeMarkerPortletPreferences.setValue("rootLayoutType", "select") />
	<#assign VOID = freeMarkerPortletPreferences.setValue("includedLayouts", "all") />
	<#assign VOID = freeMarkerPortletPreferences.setValue("nestedChildren", "false") />

	<@liferay.navigation_menu
		default_preferences="${freeMarkerPortletPreferences}"
		instance_id=instance_id
	/>
	<#assign VOID = freeMarkerPortletPreferences.reset() />
</#macro>

<#-- COMMENT: define custom macro end -->

<#-- COMMENT: changes end -->
