<#--
This file allows you to override and define new FreeMarker variables.
-->

<#-- Global Vars -->

<#assign 
	site_default_locale_id = localeUtil.toLanguageId(themeDisplay.getSiteDefaultLocale())
	url_prefix = themeDisplay.getURLCurrent()?remove_ending( themeDisplay.getLayout().getFriendlyURL() )
/>

<#-- Assign Theme Settings -->
<#assign
	<#-- Site Title -->
	custom_site_name = getterUtil.getString(theme_settings["custom-site-name"])
	custom_footer_menu_title = getterUtil.getString(theme_settings["custom-footer-menu-title"])

	<#-- Contact Info Settings -->
	contact_phone_number = getterUtil.getString(theme_settings["contact-phone-number"])
/>

<#-- Assign Fallback Site Name -->
<#if !custom_site_name?has_content>
	<#assign custom_site_name = site_name>
</#if>
